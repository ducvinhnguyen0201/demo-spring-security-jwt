package com.example.springsecuritydemo.customer;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {

    private static final List<Customer> CUSTOMERS = Arrays.asList(
            new Customer(1, "James Bond"),
            new Customer(2, "Maria Jones"),
            new Customer(3, "Anna Smith")
    );

    @GetMapping(path = "{customerId}")
    public Customer getCustomers(@PathVariable("customerId") Integer customerId){
        return CUSTOMERS.stream()
                .filter(customer -> customerId.equals(customer.getCustomerId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Customer "+ customerId + " does not exist"));
    }
}
