package com.example.springsecuritydemo.customer;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("management/api/v1/customers")
public class CustomerManagementController {

    private static final List<Customer> CUSTOMERS = Arrays.asList(
            new Customer(1, "James Bond"),
            new Customer(2, "Maria Jones"),
            new Customer(3, "Anna Smith")
    );

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN, ROLE,ADMINTRAINEE')")
     public List<Customer> getAllCustomers() {
        System.out.println("getAllCustomers");
         return CUSTOMERS;
     }

    @PostMapping
    @PreAuthorize("hasAuthority('student:write')")
     public void registerNewCustomer(@RequestBody Customer customer) {
        System.out.println("registerNewCustomer");
        System.out.println(customer);
     }

     @DeleteMapping(path = "{customerId}")
     @PreAuthorize("hasAuthority('student:write')")
     public void deleteCustomer(@PathVariable Integer customerId)  {
        System.out.println("deleteCustomer");
        System.out.println(customerId);
     }

     @PutMapping(path = "{customerId}")
     @PreAuthorize("hasAuthority('student:write')")
     public void updateCustomer(@PathVariable("customerId") Integer customerId,@RequestBody Customer customer)   {
         System.out.println("updateCustomer");
         System.out.println(String.format("%s %s", customerId, customer));
     }
}
